# Projet OpenSSH

OpenSSH, l’objectif est de configurer un serveur OpenSSH afin de permettre la connexion d’un apprenant sur l’ordinateur d’un autre apprenant. L’authentification de l’utilisateur devra se faire par l’intermédiaire de clés OpenSSH.

## Objectifs

* Découverte des protocoles de communication sécurisés entre le client et le serveur
* 

## Dispositif pédagogique

* Travail par binôme
* Suivre les étapes une par une
* Contraintes outil : **terminal**

## Etape 1

Installation et configuration du paquet *openssh-server* sur l’ensemble des ordinateurs. Dans un 1er temps, il faudra se connecter via mot de passe sur la machine de son binôme par l’intermédiaire du terminal. 

## Etape 2

Ensuite, il faudra se connecter par l’intermédiaire d’une paire de clés OpenSSH pour éviter de saisir à chaque connexion son mot de passe.

## Etape 3

Il faudra dans un second temps utiliser *scp* afin d’envoyer un lot de fichiers sur l’ordinateur de son binôme puis par l’intermédiaire de l’outil *sftp*, il faudra les supprimer.

