## Exercice 1
Génération d’une paire de clés par l’intermédiaire de l’outil *ssh-keygen* afin de simplifier l’authentification à votre compte GitLab.

## Exercice 2
Prise en main du logiciel *seahorse* pour la gestion des clés OpenSSH.